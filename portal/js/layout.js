/* Author:

*/

$(window).load(function() {
  
  function goTo(where) {
    // Switch page
    console.log(where);
    $('.page').addClass('hidden');
    $('#' + where).removeClass('hidden');
  }
  
  // Set up top navigation
	$('nav a').click(function() {
	  if ($(this).parents('.active').length != 0) return;
	  // Switch nav
	  $('nav .active').removeClass('active');
	  $(this).parents('li').addClass('active');
	  goTo($(this).attr('href').replace('#',''));
	  return false;
	});
  
  // Set up collapsable panels
	$('aside h3')
		.addClass('ui-tabs-nav ui-widget-header ui-corner-all')
		.collapsable();
	
});

