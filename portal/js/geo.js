/* Author:

*/

var GeoServerURL = document.location.origin + "/geoserver/",
		geoapi, toolbar1, rasterLayers = [];

$(window).load(function() {

	// Set up the map
	var mapheight = $(window).height() - 100,
	  mapwidth = $(window).width() - 20;
	
	$('#swissmap').css({
	  width: mapwidth + 'px',
	  height: mapheight + 'px'
	});
	
	geoapi = new GeoAdmin.API();
	
	toolbar1 = geoapi.createMapPanel({
	  renderTo: "swissmap",
	  height: mapheight,
	  //map: new GeoAdmin.Map(),
	  tbar: new Ext.Toolbar()
	});
	
	//The complementary layer is per default the color pixelmap.
	//toolbar1.map.switchComplementaryLayer("ch.swisstopo.pixelkarte-grau", {opacity: 1});
	
	//Add layer in the map
	toolbar1.map.addLayerByName('ch.swisstopo.swissboundaries3d-bezirk-flaeche.fill');
	
	//Recenter the map and define a zoom level
	//	  Y=616000&X=174600&zoom=4
	toolbar1.map.setCenter(new OpenLayers.LonLat(616000,174600),4);
	
	//Setup the toolbar with the Measure tool
	toolbar1.getTopToolbar().add(new GeoAdmin.Measure({map: toolbar1.map}));
	
	//Load extra layers from geoServer
	fetchLayers('guest', 'guest');
	
	function make_base_auth(user, password) {
		var tok = user + ':' + password;
		var hash = btoa(tok);
		return "Basic " + hash;
	}

  function fetchLayers(user, password) {
	  $.ajax({
	      type: 'GET',
	      url: GeoServerURL + 'rest/layers.json',
	      dataType: 'json',
	      async: false,
	      data: '{}',
	      beforeSend: function(xhr) { 
	          xhr.setRequestHeader('Authorization', make_base_auth(user, password)); 
	      },
	      success: parseLayers
	  });
	}

	function parseLayers(data) {
	  if (typeof data.layers == 'undefined') {
			return alert('Invalid layer data');
	  }
	  /*{"layers":{"layer":
	    [{"name":"ffneu100cwf_ascii",
	      "href":"http:\/\/129.132.91.192:8080\/geoserver\/rest\/layers\/ffneu100cwf_ascii.json"},
	      ..]}}
	   */
	  $.each(data.layers.layer, function() {

			$('#vorkriterien ul').append(
				'<li><input type="checkbox" name="' + this.name + '" value="1">&nbsp;'
				+ this.name + '</li>'
			).find('li:last input').on('change', function() {
			
				$(this).attr('disabled','disabled');
				var n = $(this).attr('name');
				
				//Overlay GeoTiFFs
				var rastermap = new OpenLayers.Layer.WMS(
				      "PLUS-PALM:" + n, 
				      GeoServerURL + "PLUS-PALM/wms",
				      {
				          LAYERS: 'PLUS-PALM:' + n,
				          STYLES: '',
				          format: 'image/png',
				          transparent: 'true'
				      },{
				          buffer: 0,
				          displayOutsideMaxExtent: true,
				          opacity: 1.0,
				          yx : {'EPSG:21781' : true}
				      } 
				  );
	
				rasterLayers.push(rastermap);
				toolbar1.map.addLayer(rastermap)
				return false;
				
			}); // end appending to list
			
		});	// end iterating layers
	}
	
	/*
	// Layer tree
	var layertree1 = new GeoAdmin.LayerTree({
	         map: toolbar1.map,
	         renderTo: "layertree6",
	         width: 300
	     });
	*/
	
});

