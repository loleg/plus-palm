LP Thun 20 criteria 22.03.2012

library(sp)
library(lpSolve)
library(foreign)
library(spdep)

#################################################################
# 1 Datenimport, Transformation, Umbenennung
#################################################################

#------------------------------ (1.1) Lese ascii files in R - Transformation von Raster zu xy 
# Kriterien
k1_sp=read.asciigrid("beig_kcwf_ascii.asc") # Nahrungsmittelproduktion
k2_sp=read.asciigrid("gwn_t100n1cla_ascii.asc") # Grundwasserneubildung
k3_sp=read.asciigrid("tws100cwf_ascii.asc") # Grundwasserschutz
k4_sp=read.asciigrid("c_stockrekwascii.asc") #CO2 Speicherung
k5_sp=read.asciigrid("exfohuse100kw_ascii.asc") # Biodiversit�t, Vernetzung
k6_sp=read.asciigrid("ffneu100cwf_ascii.asc") # Freifl�chen ffneu100cwf_ascii.asc
k7_sp=read.asciigrid("lssgcrk_ascii.asc") # Landschaftsschutzgebiet
k8_sp=read.asciigrid("vsdomhik100rc_ascii.asc") # Erholung
k9_sp=read.asciigrid("simonisc100_ascii.asc") # Alpensicht
k10_sp=read.asciigrid("ssdhmn01_ascii.asc") # Seesicht
k11_sp=read.asciigrid("sumlaermtkk_ascii.asc") # L�rm
k12_sp=read.asciigrid("sorec100wf_ascii.asc") # Besonnung
k13_sp=read.asciigrid("oevgk_ascii.asc") # Fernerschliessung OeV
k14_sp=read.asciigrid("iv_rec_clipfw_ascii.asc") # Fernerschliessung IV
k15_sp=read.asciigrid("nspd100nkwf_ascii.asc") # N�he zu Siedlung
k16_sp=read.asciigrid("pspdraea100n_ascii.asc") # Zentrumsfunktion
k17_sp=read.asciigrid("slopedhmrec_ascii.asc") # Baukosten
k18_sp=read.asciigrid("beig_kcwf_ascii.asc") # Clusterung

# Untergebiete (Gemeindegebiete) (BR total 455)
g1_sp=read.asciigrid("ud100re01ascii.asc") # Gemeindegebiet Uetendorf (BR 25) 33
g2_sp=read.asciigrid("th100re01ascii.asc") # Gemeindegebiet Thun (BR 296) 122
g3_sp=read.asciigrid("sp100re01ascii.asc") # Gemeindegebiet Spiez (BR 59) 68
g4_sp=read.asciigrid("sb100re01ascii.asc") # Gemeindegebiet Steffisburg (BR 48) 23
g5_sp=read.asciigrid("hb100re01ascii.asc") # Gemeindegebiet Heimberg (BR 27) 25

abrt=455          # Anzahl_Bauzonen_Region_Thun

# ausgelassene Gebiete (�ber alle Kriterien)
ea1_sp=read.asciigrid("sgas100naclip_ascii.asc") # Siedlungsgebiet

# Bauzonenreserve
uebz_sp=read.asciigrid("UnueberbauteBZG40.asc")

# Untergebiete f�r Bedingungen
c1_sp=read.asciigrid("s12constraint_ascii.asc") # Grundwasserschutz S1/S2 
c2_sp=read.asciigrid("nisrc1_ascii.asc") # Schutzzonen nationaler Bedeutung
c3_sp=read.asciigrid("fffcr_ascii.asc") # Fruchtfolgefl�chen
c4_sp=read.asciigrid("wafl100con_ascii.asc") # Waldfl�chen
c5_sp=read.asciigrid("syghk100c_constraint.asc")  # Naturgefahren

sg_sp=read.asciigrid("sgas100_clip_ascii.asc") # Siedlungsgebiet

# Gewichtung (je Kriterium)
wfk1=1/17
wfk2=1/17
wfk3=1/17
wfk4=1/17
wfk5=1/17
wfk6=1/17
wfk7=1/17
wfk8=1/17
wfk9=1/17
wfk10=1/17
wfk11=1/17
wfk12=1/17
wfk13=1/17
wfk14=1/17
wfk15=1/17
wfk16=1/17
wfk17=1/17
wfk17=1/17

wk1_sp=read.asciigrid("wfrtk1_011_ascii.asc")
wk2_sp=read.asciigrid("wfrtk2_0013_ascii.asc")
wk3_sp=read.asciigrid("wfrtk3_0117_ascii.asc")
wk4_sp=read.asciigrid("wfk4_007_ascii.asc")
wk5_sp=read.asciigrid("wfrtk6_013_ascii.asc")
wk6_sp=read.asciigrid("wfrtk07_006_ascii.asc")
wk7_sp=read.asciigrid("wfrtk8_002_ascii.asc")
wk8_sp=read.asciigrid("wfrtk8_002_ascii.asc")
wk9_sp=read.asciigrid("wfrtk14_00075_ascii.asc")
wk10_sp=read.asciigrid("wfrtk14_00075_ascii.asc")
wk11_sp=read.asciigrid("wfrtk8_002_ascii.asc")
wk12_sp=read.asciigrid("wfrtk14_00075_ascii.asc")
wk13_sp=read.asciigrid("wfrtk15_01_ascii.asc")
wk14_sp=read.asciigrid("wfrtk16_005_ascii.asc")
wk15_sp=read.asciigrid("wfk17_006_ascii.asc")
wk16_sp=read.asciigrid("wfk18_005_ascii.asc")
wk17_sp=read.asciigrid("wfk19_004_ascii.asc")

#------------------------------ (1.2) Converting each new file "..._sp" into a data frame object to facilitate handling of the data 
k1_sp_df=as.data.frame(k1_sp)
k2_sp_df=as.data.frame(k2_sp)
k3_sp_df=as.data.frame(k3_sp)
k4_sp_df=as.data.frame(k4_sp)
k5_sp_df=as.data.frame(k5_sp)
k6_sp_df=as.data.frame(k6_sp)
k7_sp_df=as.data.frame(k7_sp)
k8_sp_df=as.data.frame(k8_sp)
k9_sp_df=as.data.frame(k9_sp)
k10_sp_df=as.data.frame(k10_sp)
k11_sp_df=as.data.frame(k11_sp)
k12_sp_df=as.data.frame(k12_sp)
k13_sp_df=as.data.frame(k13_sp)
k14_sp_df=as.data.frame(k14_sp)
k15_sp_df=as.data.frame(k15_sp)
k16_sp_df=as.data.frame(k16_sp)
k17_sp_df=as.data.frame(k17_sp)
k18_sp_df=as.data.frame(k18_sp)

g1_sp_df=as.data.frame(g1_sp)
g2_sp_df=as.data.frame(g2_sp)
g3_sp_df=as.data.frame(g3_sp)
g4_sp_df=as.data.frame(g4_sp)
g5_sp_df=as.data.frame(g5_sp)

c1_sp_df=as.data.frame(c1_sp)
c2_sp_df=as.data.frame(c2_sp)
c3_sp_df=as.data.frame(c3_sp)
c4_sp_df=as.data.frame(c4_sp)
c5_sp_df=as.data.frame(c5_sp)

ea1_sp_df=as.data.frame(ea1_sp)

sg_sp_df=as.data.frame(sg_sp)

wk1_sp_df=as.data.frame(wk1_sp)
wk2_sp_df=as.data.frame(wk2_sp)
wk3_sp_df=as.data.frame(wk3_sp)
wk4_sp_df=as.data.frame(wk4_sp)
wk5_sp_df=as.data.frame(wk5_sp)
wk6_sp_df=as.data.frame(wk6_sp)
wk7_sp_df=as.data.frame(wk7_sp)
wk8_sp_df=as.data.frame(wk8_sp)
wk9_sp_df=as.data.frame(wk9_sp)
wk10_sp_df=as.data.frame(wk10_sp)
wk11_sp_df=as.data.frame(wk11_sp)
wk12_sp_df=as.data.frame(wk12_sp)
wk13_sp_df=as.data.frame(wk13_sp)
wk14_sp_df=as.data.frame(wk14_sp)
wk15_sp_df=as.data.frame(wk15_sp)
wk16_sp_df=as.data.frame(wk16_sp)
wk17_sp_df=as.data.frame(wk17_sp)

#------------------------------ (1.3) Benennung der Kollonen
names(k1_sp_df)=c("cell","X","Y")
names(k2_sp_df)=c("cell","X","Y")
names(k3_sp_df)=c("cell","X","Y")
names(k4_sp_df)=c("cell","X","Y")
names(k5_sp_df)=c("cell","X","Y")
names(k6_sp_df)=c("cell","X","Y")
names(k7_sp_df)=c("cell","X","Y")
names(k8_sp_df)=c("cell","X","Y")
names(k9_sp_df)=c("cell","X","Y")
names(k10_sp_df)=c("cell","X","Y")
names(k11_sp_df)=c("cell","X","Y")
names(k12_sp_df)=c("cell","X","Y")
names(k13_sp_df)=c("cell","X","Y")
names(k14_sp_df)=c("cell","X","Y")
names(k15_sp_df)=c("cell","X","Y")
names(k16_sp_df)=c("cell","X","Y")
names(k17_sp_df)=c("cell","X","Y")

names(g1_sp_df)=c("cell","X","Y")
names(g2_sp_df)=c("cell","X","Y")
names(g3_sp_df)=c("cell","X","Y")
names(g4_sp_df)=c("cell","X","Y")
names(g5_sp_df)=c("cell","X","Y")

names(c1_sp_df)=c("cell","X","Y")
names(c2_sp_df)=c("cell","X","Y")
names(c3_sp_df)=c("cell","X","Y")
names(c4_sp_df)=c("cell","X","Y")
names(c5_sp_df)=c("cell","X","Y")

names(ea1_sp_df)=c("cell","X","Y")

names(sg_sp_df)=c("cell","X","Y")

names(wk1_sp_df)=c("cell","X","Y")
names(wk2_sp_df)=c("cell","X","Y")
names(wk3_sp_df)=c("cell","X","Y")
names(wk4_sp_df)=c("cell","X","Y")
names(wk5_sp_df)=c("cell","X","Y")
names(wk6_sp_df)=c("cell","X","Y")
names(wk7_sp_df)=c("cell","X","Y")
names(wk8_sp_df)=c("cell","X","Y")
names(wk9_sp_df)=c("cell","X","Y")
names(wk10_sp_df)=c("cell","X","Y")
names(wk11_sp_df)=c("cell","X","Y")
names(wk12_sp_df)=c("cell","X","Y")
names(wk13_sp_df)=c("cell","X","Y")
names(wk14_sp_df)=c("cell","X","Y")
names(wk15_sp_df)=c("cell","X","Y")
names(wk16_sp_df)=c("cell","X","Y")
names(wk17_sp_df)=c("cell","X","Y")

#------------------------------ Gewichtungsfaktor f�r Kriterium
wk1=rep(wfk1,nrow(k1_sp_df))
wk1_sp_df=data.frame(wk1,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk2=rep(wfk2,nrow(k1_sp_df))
wk2_sp_df=data.frame(wk2,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk3=rep(wfk3,nrow(k1_sp_df))
wk3_sp_df=data.frame(wk3,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk4=rep(wfk4,nrow(k1_sp_df))
wk4_sp_df=data.frame(wk4,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk5=rep(wfk5,nrow(k1_sp_df))
wk5_sp_df=data.frame(wk5,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk6=rep(wfk6,nrow(k1_sp_df))
wk6_sp_df=data.frame(wk6,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk7=rep(wfk7,nrow(k1_sp_df))
wk7_sp_df=data.frame(wk7,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk8=rep(wfk1,nrow(k1_sp_df))
wk8_sp_df=data.frame(wk8,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk9=rep(wfk9,nrow(k1_sp_df))
wk9_sp_df=data.frame(wk9,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk10=rep(wfk10,nrow(k1_sp_df))
wk10_sp_df=data.frame(wk10,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk11=rep(wfk11,nrow(k1_sp_df))
wk11_sp_df=data.frame(wk11,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk12=rep(wfk12,nrow(k1_sp_df))
wk12_sp_df=data.frame(wk12,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk13=rep(wfk13,nrow(k1_sp_df))
wk13_sp_df=data.frame(wk13,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk14=rep(wfk14,nrow(k1_sp_df))
wk14_sp_df=data.frame(wk14,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk15=rep(wfk15,nrow(k1_sp_df))
wk15_sp_df=data.frame(wk15,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk16=rep(wfk16,nrow(k1_sp_df))
wk16_sp_df=data.frame(wk16,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk17=rep(wfk17,nrow(k1_sp_df))
wk17_sp_df=data.frame(wk17,X=k1_sp_df$X,Y=k1_sp_df$Y)
wk18=rep(wfk17,nrow(k1_sp_df))
wk18_sp_df=data.frame(wk18,X=k1_sp_df$X,Y=k1_sp_df$Y)

#------------------------------ (1.4) visualizing twk1=rep(wfk1,nrow(k1_sp_df))
image(k1_sp)
image(k2_sp)
image(k3_sp)
image(k4_sp)
image(k5_sp)
image(k6_sp)
image(k7_sp)
image(k8_sp)
image(k9_sp)
image(k10_sp)
image(k11_sp)
image(k12_sp)
image(k13_sp)
image(k14_sp)
image(k15_sp)
image(k16_sp)
image(k17_sp)
image(k18_sp)

image(g1_sp)
image(g2_sp)
image(g3_sp)
image(g4_sp)
image(g5_sp)

image(c1_sp)
image(c2_sp)
image(c3_sp)
image(c4_sp)
image(c5_sp)

image(ea1_sp)

image(sg_sp)

#################################################################
# 2 Bestimmt Fl�chen aus der Analyse ausschliessen
#################################################################
#------------------------------------ (2.1) leave an area out
global_coords=cbind(k1_sp_df$X,k1_sp_df$Y)
global_coords_sp=SpatialPoints(cbind(k1_sp_df$X,k1_sp_df$Y))

ea1_ones=ea1_sp_df[ea1_sp_df$cell==1,]#selektiert nur Werte =1
ea1_ones_sp=SpatialPoints(cbind(ea1_ones$X,ea1_ones$Y))
ea1_overlap=zerodist2(global_coords_sp,ea1_ones_sp)

k1_sp_df_ea=k1_sp_df[ea1_overlap[,1],]
k2_sp_df_ea=k2_sp_df[ea1_overlap[,1],]
k3_sp_df_ea=k3_sp_df[ea1_overlap[,1],]
k4_sp_df_ea=k4_sp_df[ea1_overlap[,1],]
k5_sp_df_ea=k5_sp_df[ea1_overlap[,1],]
k6_sp_df_ea=k6_sp_df[ea1_overlap[,1],]
k7_sp_df_ea=k7_sp_df[ea1_overlap[,1],]
k8_sp_df_ea=k8_sp_df[ea1_overlap[,1],]
k9_sp_df_ea=k9_sp_df[ea1_overlap[,1],]
k10_sp_df_ea=k10_sp_df[ea1_overlap[,1],]
k11_sp_df_ea=k11_sp_df[ea1_overlap[,1],]
k12_sp_df_ea=k12_sp_df[ea1_overlap[,1],]
k13_sp_df_ea=k13_sp_df[ea1_overlap[,1],]
k14_sp_df_ea=k14_sp_df[ea1_overlap[,1],]
k15_sp_df_ea=k15_sp_df[ea1_overlap[,1],]
k16_sp_df_ea=k16_sp_df[ea1_overlap[,1],]
k17_sp_df_ea=k17_sp_df[ea1_overlap[,1],]
k18_sp_df_ea=k18_sp_df[ea1_overlap[,1],]

wk1_sp_df_ea=wk1_sp_df[ea1_overlap[,1],]
wk2_sp_df_ea=wk2_sp_df[ea1_overlap[,1],]
wk3_sp_df_ea=wk3_sp_df[ea1_overlap[,1],]
wk4_sp_df_ea=wk4_sp_df[ea1_overlap[,1],]
wk5_sp_df_ea=wk5_sp_df[ea1_overlap[,1],]
wk6_sp_df_ea=wk6_sp_df[ea1_overlap[,1],]
wk7_sp_df_ea=wk7_sp_df[ea1_overlap[,1],]
wk8_sp_df_ea=wk8_sp_df[ea1_overlap[,1],]
wk9_sp_df_ea=wk9_sp_df[ea1_overlap[,1],]
wk10_sp_df_ea=wk10_sp_df[ea1_overlap[,1],]
wk11_sp_df_ea=wk11_sp_df[ea1_overlap[,1],]
wk12_sp_df_ea=wk12_sp_df[ea1_overlap[,1],]
wk13_sp_df_ea=wk13_sp_df[ea1_overlap[,1],]
wk14_sp_df_ea=wk14_sp_df[ea1_overlap[,1],]
wk15_sp_df_ea=wk15_sp_df[ea1_overlap[,1],]
wk16_sp_df_ea=wk16_sp_df[ea1_overlap[,1],]
wk17_sp_df_ea=wk17_sp_df[ea1_overlap[,1],]
wk18_sp_df_ea=wk18_sp_df[ea1_overlap[,1],]

c1_sp_df_ea=c1_sp_df[ea1_overlap[,1],]
c2_sp_df_ea=c2_sp_df[ea1_overlap[,1],]
c3_sp_df_ea=c3_sp_df[ea1_overlap[,1],]
c4_sp_df_ea=c4_sp_df[ea1_overlap[,1],]
c5_sp_df_ea=c5_sp_df[ea1_overlap[,1],]

#################################################################
# Layer wo Zielwert erreicht werden muss
#################################################################
 
base_layer_df=as.data.frame(cbind(k1_sp_df$X,k1_sp_df$Y))
base_layer_SP=SpatialPoints(c1_sp_df[,2:3])#2:3 Koordinaten! 

c1_ones1=which(c1_sp_df$cell==1)# Grundwasserschutz S1/S2
c2_ones1=which(c2_sp_df$cell==1)# Schutzzonen nationaler Bedeutung
c3_ones1=which(c3_sp_df$cell==1)# Fruchtfolgefl�chen
c4_ones1=which(c4_sp_df$cell==1)# Waldfl�chen
c5_ones1=which(c5_sp_df$cell==1)# Naturgefahren

sg_ones1=which(sg_sp_df$cell==1)# Siedlungsgebiet

#------------------------------------Erreichungsgebiet f�r Zielwerte der SF
coords_ones_c=base_layer_df[c(c1_ones1,c2_ones1,c4_ones1,c5_ones1,sg_ones1),] #,c3_ones1
coords_ones_c_SP=SpatialPoints(coords_ones_c)

overlapping_base_layer_ones_c=zerodist2(base_layer_SP,coords_ones_c_SP)
merged_layer_df=data.frame(cell=rep(0,nrow(base_layer_df)),X=base_layer_df$V1,Y=base_layer_df$V2)
merged_layer_df[overlapping_base_layer_ones_c[,1],1]=1
merged_layer_df_2=merged_layer_df

coordinates(merged_layer_df_2)=~X+Y #macht es zu SpatialPointsDataFrame
gridded(merged_layer_df_2)= TRUE
merged_layer_df_2_grid = as(merged_layer_df_2, "SpatialGridDataFrame") # to full grid
egzwsf=merged_layer_df_2_grid
image(merged_layer_df_2_grid)

#write.asciigrid(merged_layer_df_2_grid,"merged_layer_df_2_grid.asc")

#################################################################
# 3 Standardisation der Kriterien
#################################################################

#------------------------------------ (3.1) Lineare Transformation um sicher zu sein, dass alle Werte zwischen 0 und 1
k1_sp_df_ea_s=data.frame(k1_stand=(k1_sp_df_ea[,1]-min(k1_sp_df_ea[,1]))/(max(k1_sp_df_ea[,1])-min(k1_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k2_sp_df_ea_s=data.frame(k2_stand=(k2_sp_df_ea[,1]-min(k2_sp_df_ea[,1]))/(max(k2_sp_df_ea[,1])-min(k2_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k3_sp_df_ea_s=data.frame(k3_stand=(k3_sp_df_ea[,1]-min(k3_sp_df_ea[,1]))/(max(k3_sp_df_ea[,1])-min(k3_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k4_sp_df_ea_s=data.frame(k4_stand=(k4_sp_df_ea[,1]-min(k4_sp_df_ea[,1]))/(max(k4_sp_df_ea[,1])-min(k4_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k5_sp_df_ea_s=data.frame(k5_stand=(k5_sp_df_ea[,1]-min(k5_sp_df_ea[,1]))/(max(k5_sp_df_ea[,1])-min(k5_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k6_sp_df_ea_s=data.frame(k6_stand=(k6_sp_df_ea[,1]-min(k6_sp_df_ea[,1]))/(max(k6_sp_df_ea[,1])-min(k6_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k7_sp_df_ea_s=data.frame(k7_stand=(k7_sp_df_ea[,1]-min(k7_sp_df_ea[,1]))/(max(k7_sp_df_ea[,1])-min(k7_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k8_sp_df_ea_s=data.frame(k8_stand=(k8_sp_df_ea[,1]-min(k8_sp_df_ea[,1]))/(max(k8_sp_df_ea[,1])-min(k8_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k9_sp_df_ea_s=data.frame(k9_stand=(k9_sp_df_ea[,1]-min(k9_sp_df_ea[,1]))/(max(k9_sp_df_ea[,1])-min(k9_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k10_sp_df_ea_s=data.frame(k10_stand=(k10_sp_df_ea[,1]-min(k10_sp_df_ea[,1]))/(max(k10_sp_df_ea[,1])-min(k10_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k11_sp_df_ea_s=data.frame(k11_stand=(k11_sp_df_ea[,1]-min(k11_sp_df_ea[,1]))/(max(k11_sp_df_ea[,1])-min(k11_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k12_sp_df_ea_s=data.frame(k12_stand=(k12_sp_df_ea[,1]-min(k12_sp_df_ea[,1]))/(max(k12_sp_df_ea[,1])-min(k12_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k13_sp_df_ea_s=data.frame(k13_stand=(k13_sp_df_ea[,1]-min(k13_sp_df_ea[,1]))/(max(k13_sp_df_ea[,1])-min(k13_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k14_sp_df_ea_s=data.frame(k14_stand=(k14_sp_df_ea[,1]-min(k14_sp_df_ea[,1]))/(max(k14_sp_df_ea[,1])-min(k14_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k15_sp_df_ea_s=data.frame(k15_stand=(k15_sp_df_ea[,1]-min(k15_sp_df_ea[,1]))/(max(k15_sp_df_ea[,1])-min(k15_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k16_sp_df_ea_s=data.frame(k16_stand=(k16_sp_df_ea[,1]-min(k16_sp_df_ea[,1]))/(max(k16_sp_df_ea[,1])-min(k16_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k17_sp_df_ea_s=data.frame(k17_stand=(k17_sp_df_ea[,1]-min(k17_sp_df_ea[,1]))/(max(k17_sp_df_ea[,1])-min(k17_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)

#to map it
#k13_sp_df_s_m=data.frame(value=k13_sp_df_s,X=k13_sp_df_s$X,Y=k13_sp_df_s$Y) #braucht es um ein SpatialPixelsDataFrame zu bilden, welches mit image gemapped werden kann
#coordinates(k13_sp_df_s_m)=~X+Y
#gridded(k13_sp_df_s_m)= TRUE
#k13_sp_df_s_m_grid = as(k13_sp_df_s_m, "SpatialGridDataFrame") # to full grid
#image(k13_sp_df_s_m_grid)

#write.asciigrid(k13_sp_df_s_m_grid,"k13sn.asc")

#################################################################
# 3a Sensitivit�tsanalyse erste Stufe Kriterien
#################################################################

#------------------------------------- Aufsummiert

added17k_sp_df_ea_s_a=data.frame((k_aufsummiert=((k1_sp_df_ea_s[,1])+(k2_sp_df_ea_s[,1])+(k3_sp_df_ea_s[,1])+(k4_sp_df_ea_s[,1])+(k5_sp_df_ea_s[,1])+(k6_sp_df_ea_s[,1])+(k7_sp_df_ea_s[,1])+(k8_sp_df_ea_s[,1])+(k9_sp_df_ea_s[,1])+(k10_sp_df_ea_s[,1])+(k11_sp_df_ea_s[,1])+(k12_sp_df_ea_s[,1])+(k13_sp_df_ea_s[,1])+(k14_sp_df_ea_s[,1])+(k15_sp_df_ea_s[,1])+(k16_sp_df_ea_s[,1])+(k17_sp_df_ea_s[,1]))),X=k1_sp_df_ea_s$X,Y=k1_sp_df_ea_s$Y)

added17k_sp_df_ea_s_a_m=data.frame(value=added17k_sp_df_ea_s_a,X=k1_sp_df_ea_s$X,Y=k1_sp_df_ea_s$Y) #braucht es um ein SpatialPixelsDataFrame zu bilden, welches mit image gemapped werden kann

coordinates(added17k_sp_df_ea_s_a_m)=~X+Y
gridded(added17k_sp_df_ea_s_a_m)= TRUE
added17k_sp_df_ea_s_a_m_grid = as(added17k_sp_df_ea_s_a_m, "SpatialGridDataFrame") # to full grid
image(added17k_sp_df_ea_s_a_m_grid)

write.asciigrid(added17k_sp_df_ea_s_a_m_grid,"added17k_sp_df_ea_s_a_m_grid.asc")
write.dbf(added17k_sp_df_ea_s_a_m_grid,"added17k_sp_df_ea_s_a_m_grid.dbf")

#------------------------------------- Darstellung aller Kriterienwerte vom Kleinsten zum Gr�ssten

k1_sp_df_ea_s_auds=k1_sp_df_ea_s[order(k1_sp_df_ea_s[,1],decreasing = F),]
k1_sp_df_ea_s_auds=k1_sp_df_ea_s[sort(k1_sp_df_ea_s[,1],decreasing = F),]

k1_sp_df_ea_s_auds=(sort(k1_sp_df_ea_s[,1],decreasing = F))
k2_sp_df_ea_s_auds=(sort(k2_sp_df_ea_s[,1],decreasing = F))
k3_sp_df_ea_s_auds=(sort(k3_sp_df_ea_s[,1],decreasing = F))
k4_sp_df_ea_s_auds=(sort(k4_sp_df_ea_s[,1],decreasing = F))
k5_sp_df_ea_s_auds=(sort(k5_sp_df_ea_s[,1],decreasing = F))
k6_sp_df_ea_s_auds=(sort(k6_sp_df_ea_s[,1],decreasing = F))
k7_sp_df_ea_s_auds=(sort(k7_sp_df_ea_s[,1],decreasing = F))
k8_sp_df_ea_s_auds=(sort(k8_sp_df_ea_s[,1],decreasing = F))
k9_sp_df_ea_s_auds=(sort(k9_sp_df_ea_s[,1],decreasing = F))
k10_sp_df_ea_s_auds=(sort(k10_sp_df_ea_s[,1],decreasing = F))
k11_sp_df_ea_s_auds=(sort(k11_sp_df_ea_s[,1],decreasing = F))
k12_sp_df_ea_s_auds=(sort(k12_sp_df_ea_s[,1],decreasing = F))
k13_sp_df_ea_s_auds=(sort(k13_sp_df_ea_s[,1],decreasing = F))
k14_sp_df_ea_s_auds=(sort(k14_sp_df_ea_s[,1],decreasing = F))
k15_sp_df_ea_s_auds=(sort(k15_sp_df_ea_s[,1],decreasing = F))
k16_sp_df_ea_s_auds=(sort(k16_sp_df_ea_s[,1],decreasing = F))
k17_sp_df_ea_s_auds=(sort(k17_sp_df_ea_s[,1],decreasing = F))
k_aufsummiert_auds=(sort(k_aufsummiert,decreasing = F))

#length(k17_sp_df_ea_s_auds)

#hier noch Anzahlbauzonenreserven +100%


# Kriterientabelle erstellen
#------------------------------------- standardisiert durch Wertfunktion
kriterientabelle=data.frame(k1=(k1_sp_df_ea_s_auds),k2=(k2_sp_df_ea_s_auds),k3=(k3_sp_df_ea_s_auds),k4=(k4_sp_df_ea_s_auds),k5=(k5_sp_df_ea_s_auds),k6=(k6_sp_df_ea_s_auds),k7=(k7_sp_df_ea_s_auds),k8=(k8_sp_df_ea_s_auds),k9=(k9_sp_df_ea_s_auds),k10=(k10_sp_df_ea_s_auds),k11=(k11_sp_df_ea_s_auds),k12=(k12_sp_df_ea_s_auds),k13=(k13_sp_df_ea_s_auds),k14=(k14_sp_df_ea_s_auds),k15=(k15_sp_df_ea_s_auds),k16=(k16_sp_df_ea_s_auds),k17=(k17_sp_df_ea_s_auds),kadd=(k_aufsummiert_auds),X=k1_sp_df_ea_s$X,Y=k1_sp_df_ea_s$Y)

coordinates(kriterientabelle)=~X+Y
gridded(kriterientabelle) = TRUE
kriterientabelle = as(kriterientabelle, "SpatialGridDataFrame")

write.dbf(kriterientabelle,file="Sensitivit�tsanalyse erste Stufe Kriterien.dbf")

#################################################################
# 4 Gewichtung der Ziele
#################################################################
#------------------------------------ (4.1) Gewichtung
k1_sp_df_ea_s_w=data.frame((k1_weighted=(k1_sp_df_ea_s[,1]*wk1_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k2_sp_df_ea_s_w=data.frame((k2_weighted=(k2_sp_df_ea_s[,1]*wk2_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k3_sp_df_ea_s_w=data.frame((k3_weighted=(k3_sp_df_ea_s[,1]*wk3_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k4_sp_df_ea_s_w=data.frame((k4_weighted=(k4_sp_df_ea_s[,1]*wk4_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k5_sp_df_ea_s_w=data.frame((k5_weighted=(k5_sp_df_ea_s[,1]*wk5_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k6_sp_df_ea_s_w=data.frame((k6_weighted=(k6_sp_df_ea_s[,1]*wk6_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k7_sp_df_ea_s_w=data.frame((k7_weighted=(k7_sp_df_ea_s[,1]*wk7_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k8_sp_df_ea_s_w=data.frame((k8_weighted=(k8_sp_df_ea_s[,1]*wk8_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k9_sp_df_ea_s_w=data.frame((k9_weighted=(k9_sp_df_ea_s[,1]*wk9_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k10_sp_df_ea_s_w=data.frame((k10_weighted=(k10_sp_df_ea_s[,1]*wk10_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k11_sp_df_ea_s_w=data.frame((k11_weighted=(k11_sp_df_ea_s[,1]*wk11_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k12_sp_df_ea_s_w=data.frame((k12_weighted=(k12_sp_df_ea_s[,1]*wk12_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k13_sp_df_ea_s_w=data.frame((k13_weighted=(k13_sp_df_ea_s[,1]*wk13_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k14_sp_df_ea_s_w=data.frame((k14_weighted=(k14_sp_df_ea_s[,1]*wk14_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k15_sp_df_ea_s_w=data.frame((k15_weighted=(k15_sp_df_ea_s[,1]*wk15_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k16_sp_df_ea_s_w=data.frame((k16_weighted=(k16_sp_df_ea_s[,1]*wk16_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k17_sp_df_ea_s_w=data.frame((k17_weighted=(k17_sp_df_ea_s[,1]*wk17_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k18_sp_df_ea_s_w=data.frame((k18_weighted=(k18_sp_df_ea[,1]*wk18_sp_df_ea[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)

#to map it
#k10_sp_df_ea_s_w_m=data.frame(value=k10_sp_df_ea_s_w,X=k15_sp_df_ea_s_w$X,Y=k10_sp_df_ea_s_w$Y) #braucht es um ein SpatialPixelsDataFrame zu bilden, welches mit image gemapped werden kann
#coordinates(k10_sp_df_ea_s_w_m)=~X+Y
#gridded(k10_sp_df_ea_s_w_m)= TRUE
#k10_sp_df_ea_s_w_m_grid = as(k10_sp_df_ea_s_w_m, "SpatialGridDataFrame") # to full grid
#image(k10_sp_df_ea_s_w_m_grid)

#################################################################
# 4a Sensitivit�tsanalyse 2 Stufe f�r Gewichtung der Ziele
#################################################################
#------------------------------------ (4.1) Gewichtungslayer
saf=0.1 #Gewichtungsfaktor
while(saf<10){

anzk=17 #Anzahl Kriterien inkl. Klusterkriterium

kvwfsa=rep((saf/(saf+anzk-1)),nrow(k1_sp_df_ea)) #Gewichtungsfaktor f�r variables Kriterium
kfwfsa=rep(((1-(saf/(saf+anzk-1)))/(anzk-1)),nrow(k1_sp_df_ea))		# Gewichtungsfaktor f�r fixe Kriterien

kvwfsa_salayer=data.frame(kvwfsa,X=k1_sp_df_ea_s$X,Y=k1_sp_df_ea_s$Y)
kfwfsa_salayer=data.frame(kfwfsa,X=k1_sp_df_ea_s$X,Y=k1_sp_df_ea_s$Y)

#------------------------------------ (4.1) Gewichtung der Ziele
k1_sp_df_ea_s_wsa=data.frame((k1_weighted=(k1_sp_df_ea_s[,1]*kvwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k2_sp_df_ea_s_wsa=data.frame((k2_weighted=(k2_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k3_sp_df_ea_s_wsa=data.frame((k3_weighted=(k3_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k4_sp_df_ea_s_wsa=data.frame((k4_weighted=(k4_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k5_sp_df_ea_s_wsa=data.frame((k5_weighted=(k5_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k6_sp_df_ea_s_wsa=data.frame((k6_weighted=(k6_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k7_sp_df_ea_s_wsa=data.frame((k7_weighted=(k7_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k8_sp_df_ea_s_wsa=data.frame((k8_weighted=(k8_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k9_sp_df_ea_s_wsa=data.frame((k9_weighted=(k9_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k10_sp_df_ea_s_wsa=data.frame((k10_weighted=(k10_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k11_sp_df_ea_s_wsa=data.frame((k11_weighted=(k11_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k12_sp_df_ea_s_wsa=data.frame((k12_weighted=(k12_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k13_sp_df_ea_s_wsa=data.frame((k13_weighted=(k13_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k14_sp_df_ea_s_wsa=data.frame((k14_weighted=(k14_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k15_sp_df_ea_s_wsa=data.frame((k15_weighted=(k15_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k16_sp_df_ea_s_wsa=data.frame((k16_weighted=(k16_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)
k17_sp_df_ea_s_wsa=data.frame((k17_weighted=(k17_sp_df_ea_s[,1]*kfwfsa_salayer[,1])),X=k1_sp_df_ea$X,Y=k1_sp_df_ea$Y)

C_right_hand=rep(0,1)
C_right_hand[1]=455
Ncells=nrow(k1_sp_df_ea_s_wsa)
COb=rep(0,Ncells)

for (i in 1:Ncells)
COb[i]=k1_sp_df_ea_s_wsa[i,1]+k2_sp_df_ea_s_wsa[i,1]+k3_sp_df_ea_s_wsa[i,1]+k4_sp_df_ea_s_wsa[i,1]+k5_sp_df_ea_s_wsa[i,1]+k6_sp_df_ea_s_wsa[i,1]+k7_sp_df_ea_s_wsa[i,1]+k8_sp_df_ea_s_wsa[i,1]+k9_sp_df_ea_s_wsa[i,1]+k10_sp_df_ea_s_wsa[i,1]+k11_sp_df_ea_s_wsa[i,1]+k12_sp_df_ea_s_wsa[i,1]+k13_sp_df_ea_s_wsa[i,1]+k14_sp_df_ea_s_wsa[i,1]+k15_sp_df_ea_s_wsa[i,1]+k16_sp_df_ea_s_wsa[i,1]+k17_sp_df_ea_s_wsa[i,1]

Cconst=matrix(0,(1),Ncells)# initial values of the matrix

Cdir=c("=")

Cconst[1,]=rep(1,nrow(k1_sp_df_ea_s_wsa))

lp_results=lp(direction="max", objective.in=COb, const.mat=Cconst, const.dir=Cdir, const.rhs=C_right_hand,all.bin=T)

print(lp_results$solution)

#------------------------------------- Export L�sungen
lp_output_nc_df_stand=data.frame(result=lp_results$solution,X=k1_sp_df_ea_s_wsa$X,Y=k1_sp_df_ea_s_wsa$Y)

coordinates(lp_output_nc_df_stand)=~X+Y
gridded(lp_output_nc_df_stand) = TRUE

lp_output_nc_grid_stand = as(lp_output_nc_df_stand, "SpatialGridDataFrame") # to full grid
image(lp_output_nc_grid_stand)

names_file_asc=paste("C:\\Users\\J�rg Altwegg\\Desktop\\R_Plattform\\SA_Stufe2\\","SA",as.character(saf),".asc",sep="")
names_file_dbf=paste("C:\\Users\\J�rg Altwegg\\Desktop\\R_Plattform\\SA_Stufe2\\","SA",as.character(saf),".dbf",sep="")

write.asciigrid(lp_output_nc_grid_stand,names_file_asc) # L�sung 
write.dbf(lp_output_nc_grid_stand,file=names_file_dbf)

saf=saf+0.1
}

SA1=read.asciigrid("SA1.asc")
image(SA1)

SA9=read.asciigrid("SA9.asc")
image(SA9)

lp_results$solution

#################################################################
# 5 Constraints
#################################################################
#------------------------------------ (5.1) Bestimmte Anzahl Bauzonen in einer Gemeinde
coords_thun_ea1=SpatialPoints(cbind(ea1_sp_df$X,ea1_sp_df$Y))

g1_ones=g1_sp_df[g1_sp_df$cell==1,]
g1_ones_sp=SpatialPoints(cbind(g1_ones$X,g1_ones$Y)) # f�r das Package zerodist2 braucht man das Format SpatialPoints
g1_overlap=zerodist2(coords_thun_ea1,g1_ones_sp,zero=50)
g1_sp_df_ea1=ea1_sp_df[g1_overlap[,1],]

vec_g1=rep(0,nrow(ea1_sp_df))
vec_g1[c(g1_overlap[,1])]=1


g2_ones=g2_sp_df[g2_sp_df$cell==1,]
g2_ones_sp=SpatialPoints(cbind(g2_ones$X,g2_ones$Y))
g2_overlap=zerodist2(coords_thun_ea1,g2_ones_sp,zero=50)
g2_sp_df_ea1=ea1_sp_df[g2_overlap[,1],]

vec_g2=rep(0,nrow(ea1_sp_df))
vec_g2[c(g2_overlap[,1])]=1


g3_ones=g3_sp_df[g3_sp_df$cell==1,]
g3_ones_sp=SpatialPoints(cbind(g3_ones$X,g3_ones$Y))
g3_overlap=zerodist2(coords_thun_ea1,g3_ones_sp,zero=50)
g3_sp_df_ea1=ea1_sp_df[g3_overlap[,1],]

vec_g3=rep(0,nrow(ea1_sp_df))
vec_g3[c(g3_overlap[,1])]=1


g4_ones=g4_sp_df[g4_sp_df$cell==1,]
g4_ones_sp=SpatialPoints(cbind(g4_ones$X,g4_ones$Y))
g4_overlap=zerodist2(coords_thun_ea1,g4_ones_sp,zero=50)
g4_sp_df_ea1=ea1_sp_df[g4_overlap[,1],]

vec_g4=rep(0,nrow(ea1_sp_df))
vec_g4[c(g4_overlap[,1])]=1


g5_ones=g5_sp_df[g5_sp_df$cell==1,]
g5_ones_sp=SpatialPoints(cbind(g5_ones$X,g5_ones$Y))
g5_overlap=zerodist2(coords_thun_ea1,g5_ones_sp,zero=50)
g5_sp_df_ea1=ea1_sp_df[g5_overlap[,1],]

vec_g5=rep(0,nrow(ea1_sp_df))
vec_g5[c(g5_overlap[,1])]=1

#------------------------------------ (4.2) no cells in a area
c1_ones=c1_sp_df[c1_sp_df$cell==1,]
c1_ones_sp=SpatialPoints(cbind(c1_ones$X,c1_ones$Y))
c1_overlap=zerodist2(coords_thun_ea1,c1_ones_sp,zero=50)
c1_sp_df_ea1=ea1_sp_df[c1_overlap[,1],]

vec_c1=rep(0,nrow(ea1_sp_df))
vec_c1[c(c1_overlap[,1])]=1

c2_ones=c2_sp_df[c2_sp_df$cell==1,]
c2_ones_sp=SpatialPoints(cbind(c2_ones$X,c2_ones$Y))
c2_overlap=zerodist2(coords_thun_ea1,c2_ones_sp,zero=50)
c2_sp_df_ea1=ea1_sp_df[c2_overlap[,1],]

vec_c2=rep(0,nrow(ea1_sp_df))
vec_c2[c(c2_overlap[,1])]=1

c3_ones=c3_sp_df[c3_sp_df$cell==1,]
c3_ones_sp=SpatialPoints(cbind(c3_ones$X,c3_ones$Y))
c3_overlap=zerodist2(coords_thun_ea1,c3_ones_sp,zero=50)
c3_sp_df_ea1=ea1_sp_df[c3_overlap[,1],]

vec_c3=rep(0,nrow(ea1_sp_df))
vec_c3[c(c3_overlap[,1])]=1

c4_ones=c4_sp_df[c4_sp_df$cell==1,]
c4_ones_sp=SpatialPoints(cbind(c4_ones$X,c4_ones$Y))
c4_overlap=zerodist2(coords_thun_ea1,c4_ones_sp,zero=50)
c4_sp_df_ea1=ea1_sp_df[c4_overlap[,1],]

vec_c4=rep(0,nrow(ea1_sp_df))
vec_c4[c(c4_overlap[,1])]=1

c5_ones=c5_sp_df[c5_sp_df$cell==1,]
c5_ones_sp=SpatialPoints(cbind(c5_ones$X,c5_ones$Y))
c5_overlap=zerodist2(coords_thun_ea1,c5_ones_sp,zero=50)
c5_sp_df_ea1=ea1_sp_df[c5_overlap[,1],]

vec_c5=rep(0,nrow(ea1_sp_df))
vec_c5[c(c5_overlap[,1])]=1

#------------------------------------ Erreichungsgebiet f�r Zielbedingungen

#------------------------------------
global_coords=cbind(k1_sp_df_ea_s_w$X,k1_sp_df_ea_s_w$Y)
global_coords_sp=SpatialPoints(cbind(k1_sp_df_ea_s_w$X,k1_sp_df_ea_s_w$Y))

egzwsf_aux=as.data.frame(egzwsf)
egzwsf_zero=egzwsf_aux[egzwsf_aux$cell==0,] #selektiert nur Werte =0
egzwsf_zero_sp=SpatialPoints(cbind(egzwsf_zero$X,egzwsf_zero$Y))
egzwsf_overlap=zerodist2(global_coords_sp,egzwsf_zero_sp)


k9_sp_df_ea_s_w_egzwsf=data.frame(value=rep(0,nrow(global_coords)),global_coords)
k9_sp_df_ea_s_w_egzwsf[egzwsf_overlap[,1],1]=k9_sp_df_ea_s_w[egzwsf_overlap[,1],1]

k10_sp_df_ea_s_w_egzwsf=data.frame(value=rep(0,nrow(global_coords)),global_coords)
k10_sp_df_ea_s_w_egzwsf[egzwsf_overlap[,1],1]=k10_sp_df_ea_s_w[egzwsf_overlap[,1],1]

k11_sp_df_ea_s_w_egzwsf=data.frame(value=rep(0,nrow(global_coords)),global_coords)
k11_sp_df_ea_s_w_egzwsf[egzwsf_overlap[,1],1]=k11_sp_df_ea_s_w[egzwsf_overlap[,1],1]

k12_sp_df_ea_s_w_egzwsf=data.frame(value=rep(0,nrow(global_coords)),global_coords)
k12_sp_df_ea_s_w_egzwsf[egzwsf_overlap[,1],1]=k12_sp_df_ea_s_w[egzwsf_overlap[,1],1]

k13_sp_df_ea_s_w_egzwsf=data.frame(value=rep(0,nrow(global_coords)),global_coords)
k13_sp_df_ea_s_w_egzwsf[egzwsf_overlap[,1],1]=k13_sp_df_ea_s_w[egzwsf_overlap[,1],1]

k14_sp_df_ea_s_w_egzwsf=data.frame(value=rep(0,nrow(global_coords)),global_coords)
k14_sp_df_ea_s_w_egzwsf[egzwsf_overlap[,1],1]=k14_sp_df_ea_s_w[egzwsf_overlap[,1],1]

k15_sp_df_ea_s_w_egzwsf=data.frame(value=rep(0,nrow(global_coords)),global_coords)
k15_sp_df_ea_s_w_egzwsf[egzwsf_overlap[,1],1]=k15_sp_df_ea_s_w[egzwsf_overlap[,1],1]

k16_sp_df_ea_s_w_egzwsf=data.frame(value=rep(0,nrow(global_coords)),global_coords)
k16_sp_df_ea_s_w_egzwsf[egzwsf_overlap[,1],1]=k16_sp_df_ea_s_w[egzwsf_overlap[,1],1]

k17_sp_df_ea_s_w_egzwsf=data.frame(value=rep(0,nrow(global_coords)),global_coords)
k17_sp_df_ea_s_w_egzwsf[egzwsf_overlap[,1],1]=k17_sp_df_ea_s_w[egzwsf_overlap[,1],1]

#------------------------------------- (5.3)Bedingungen
C_right_hand=rep(0,45)

C_right_hand[1]=455 #75   #455

C_right_hand[2]=0#33 #Ud 25
C_right_hand[3]=0#296 #Th 296
C_right_hand[4]=0#68 #SP 59
C_right_hand[5]=0#23 #SB 48
C_right_hand[6]=0#25 #HB 27

C_right_hand[7]=0
C_right_hand[8]=0
C_right_hand[9]=0
C_right_hand[10]=0
C_right_hand[11]=0

#------------------------------------- (5.4)Zielbedingungen
#--------------Prozentzahl heutige Bauzonenreserven; ganzes Gebiet, Gemeinden
zwk1=50
zwk2=50
zwk3=50
zwk4=50
zwk5=50
zwk6=50
zwk7=50
zwk8=50
zwk9=50
zwk10=50
zwk11=50
zwk12=50
zwk13=50
zwk14=50
zwk15=50
zwk16=50
zwk17=50

ar=-40
ark1=-40

#while(sum(lp_results$solution)<=1){
#while(ark1<(-37)){

zwk1a=10-ark1 # %/100!  erlaubte Abweichung
zwk2a=10-ark1
zwk3a=10-ark1
zwk4a=10-ar
zwk5a=10-ar
zwk6a=10-ar
zwk7a=10-ar
zwk8a=10-ar
zwk9a=10-ar
zwk10a=10-ar
zwk11a=10-ar
zwk12a=10-ar
zwk13a=10-ar
zwk14a=10-ar
zwk15a=10-ar
zwk16a=10-ar
zwk17a=10-ar

#-ar= Abweichungsreduktion

zwk1h=zwk1/100+((zwk1a+100)/100)        # %/100!  Nahrungsproduktion
zwk1l=zwk1/100-(zwk1a/100)

zwk2h=zwk2/100+((zwk2a+100)/100)
zwk2l=zwk2/100-(zwk2a/100)

zwk3h=zwk3/100+((zwk3a+100)/100)
zwk3l=zwk3/100-(zwk3a/100)

zwk4h=zwk4/100+((zwk4a+100)/100)
zwk4l=zwk4/100-(zwk4a/100)

zwk5h=zwk5/100+((zwk5a+100)/100)
zwk5l=zwk5/100-(zwk5a/100)

zwk6h=zwk6/100+((zwk6a+100)/100)
zwk6l=zwk6/100-(zwk6a/100)

zwk7h=zwk7/100+((zwk7a+100)/100)
zwk7l=zwk7/100-(zwk7a/100)

zwk8h=zwk8/100+((zwk8a+100)/100)
zwk8l=zwk8/100-(zwk8a/100)

zwk9h=1 #zwk9/100+((zwk9a+100)/100)#ohne
zwk9l=0 #zwk9/100-(zwk9a/100)#ohne

zwk10h=1 #zwk10/100+((zwk10a+100)/100)#ohne
zwk10l=0 #zwk10/100-(zwk10a/100)#ohne

zwk11h=1 #zwk11/100+((zwk11a+100)/100)#ohne
zwk11l=0 #zwk11/100-(zwk11a/100)#ohne

zwk12h=zwk12/100+((zwk12a+100)/100)
zwk12l=zwk12/100-(zwk12a/100)

zwk13h=zwk13/100+((zwk13a+100)/100)
zwk13l=zwk13/100-(zwk13a/100)

zwk14h=zwk14/100+((zwk14a+100)/100)
zwk14l=zwk14/100-(zwk14a/100)

zwk15h=zwk15/100+((zwk15a+100)/100)
zwk15l=zwk15/100-(zwk15a/100)

zwk16h=zwk16/100+((zwk16a+100)/100)
zwk16l=zwk16/100-(zwk16a/100)

zwk17h=zwk17/100+((zwk17a+100)/100)
zwk17l=zwk17/100-(zwk17a/100)

C_right_hand[12]=((sum(sort(k1_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k1_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk1h)+(sum(sort(k1_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))
C_right_hand[13]=((sum(sort(k1_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k1_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk1l)+sum(sort(k1_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[14]=((sum(sort(k2_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k2_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk2h)+sum(sort(k2_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[15]=((sum(sort(k2_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k2_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk2l)+sum(sort(k2_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[16]=((sum(sort(k3_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k3_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk3h)+sum(sort(k3_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[17]=((sum(sort(k3_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k3_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk3l)+sum(sort(k3_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[18]=((sum(sort(k4_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k4_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk4h)+sum(sort(k4_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[19]=((sum(sort(k4_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k4_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk4l)+sum(sort(k4_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[20]=((sum(sort(k5_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k5_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk5h)+sum(sort(k5_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[21]=((sum(sort(k5_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k5_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk5l)+sum(sort(k5_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[22]=((sum(sort(k6_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k6_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk6h)+sum(sort(k6_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[23]=((sum(sort(k6_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k6_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk6l)+sum(sort(k6_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[24]=((sum(sort(k7_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k7_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk7h)+sum(sort(k7_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[25]=((sum(sort(k7_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k7_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk7l)+sum(sort(k7_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[26]=((sum(sort(k8_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k8_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk8h)+sum(sort(k8_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[27]=((sum(sort(k8_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k8_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk8l)+sum(sort(k8_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[28]=((sum(sort(k9_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k9_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk9h)+sum(sort(k9_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[29]=((sum(sort(k9_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k9_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk9l)+sum(sort(k9_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[30]=((sum(sort(k10_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k10_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk10h)+sum(sort(k10_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[31]=((sum(sort(k10_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k10_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk10l)+sum(sort(k10_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[32]=((sum(sort(k11_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k11_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk11h)+sum(sort(k11_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])
C_right_hand[33]=((sum(sort(k11_sp_df_ea_s_w[,1],decreasing=T)[1:abrt])-sum(sort(k11_sp_df_ea_s_w[,1],decreasing=F)[1:abrt]))*zwk11l)+sum(sort(k11_sp_df_ea_s_w[,1],decreasing=F)[1:abrt])

C_right_hand[34]=((sum(sort(k12_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k12_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk12h)+sum(sort(k12_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])
C_right_hand[35]=((sum(sort(k12_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k12_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk12l)+sum(sort(k12_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])

C_right_hand[36]=((sum(sort(k13_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k13_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk13h)+sum(sort(k13_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])
C_right_hand[37]=((sum(sort(k13_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k13_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk13l)+sum(sort(k13_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])

C_right_hand[38]=((sum(sort(k14_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k14_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk14h)+sum(sort(k14_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])
C_right_hand[39]=((sum(sort(k14_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k14_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk14l)+sum(sort(k14_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])

C_right_hand[40]=((sum(sort(k15_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k15_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk15h)+sum(sort(k15_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])
C_right_hand[41]=((sum(sort(k15_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k15_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk15l)+sum(sort(k15_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])

C_right_hand[42]=((sum(sort(k16_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k16_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk16h)+sum(sort(k16_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])
C_right_hand[43]=((sum(sort(k16_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k16_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk16l)+sum(sort(k16_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])

C_right_hand[44]=((sum(sort(k17_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k17_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk17h)+sum(sort(k17_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])
C_right_hand[45]=((sum(sort(k17_sp_df_ea_s_w_egzwsf[,1],decreasing=T)[1:abrt])-sum(sort(k17_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt]))*zwk17l)+sum(sort(k17_sp_df_ea_s_w_egzwsf[,1],decreasing=F)[1:abrt])

#################################################################
# 6.1 Objektiffunktion
#################################################################

#------------------------------------ (+) Coefficients for the objective function
Ncells=nrow(k1_sp_df_ea_s_w)
COb=rep(0,Ncells)

Nconst=34

for (i in 1:Ncells){
COb[i]=k1_sp_df_ea_s_w[i,1]+k2_sp_df_ea_s_w[i,1]+k3_sp_df_ea_s_w[i,1]+k4_sp_df_ea_s_w[i,1]+k5_sp_df_ea_s_w[i,1]+k6_sp_df_ea_s_w[i,1]+k7_sp_df_ea_s_w[i,1]+k8_sp_df_ea_s_w[i,1]+k9_sp_df_ea_s_w[i,1]+k10_sp_df_ea_s_w[i,1]+k11_sp_df_ea_s_w[i,1]+k12_sp_df_ea_s_w[i,1]+k13_sp_df_ea_s_w[i,1]+k14_sp_df_ea_s_w[i,1]+k15_sp_df_ea_s_w[i,1]+k16_sp_df_ea_s_w[i,1]+k17_sp_df_ea_s_w[i,1]+k18_sp_df_ea[i,1]}
Cconst=matrix(0,(Nconst+11),Ncells)# initial values of the matrix

Cdir=c("=",rep(">=",5),rep("=",2),"<=",rep("=",2),rep(c("<=",">="),17))      #,rep(c("<=",">="),17))
# (Anz. Zellen)(5*GE- Ud, Th, Sp, Sb, Hb)(Grundwasserschutz S1/S2, Schutzzonen, Fruchtfolgefl�chen, Waldfl�chen, Naturgefahren)(17*h,l Zielbedingungen)

Cconst[1,]=rep(1,nrow(k1_sp_df_ea_s_w))

Cconst[2,]=vec_g1
Cconst[3,]=vec_g2
Cconst[4,]=vec_g3
Cconst[5,]=vec_g4
Cconst[6,]=vec_g5

Cconst[7,]=vec_c1
Cconst[8,]=vec_c2
Cconst[9,]=vec_c3
Cconst[10,]=vec_c4
Cconst[11,]=vec_c5


Cconst[12,]=k1_sp_df_ea_s_w[,1]
Cconst[13,]=k1_sp_df_ea_s_w[,1]

Cconst[14,]=k2_sp_df_ea_s_w[,1]
Cconst[15,]=k2_sp_df_ea_s_w[,1]

Cconst[16,]=k3_sp_df_ea_s_w[,1]
Cconst[17,]=k3_sp_df_ea_s_w[,1]

Cconst[18,]=k4_sp_df_ea_s_w[,1]
Cconst[19,]=k4_sp_df_ea_s_w[,1]

Cconst[20,]=k5_sp_df_ea_s_w[,1]
Cconst[21,]=k5_sp_df_ea_s_w[,1]

Cconst[22,]=k6_sp_df_ea_s_w[,1]
Cconst[23,]=k6_sp_df_ea_s_w[,1]

Cconst[24,]=k7_sp_df_ea_s_w[,1]
Cconst[25,]=k7_sp_df_ea_s_w[,1]

Cconst[26,]=k8_sp_df_ea_s_w[,1]
Cconst[27,]=k8_sp_df_ea_s_w[,1]

Cconst[28,]=k9_sp_df_ea_s_w[,1]
Cconst[29,]=k9_sp_df_ea_s_w[,1]

Cconst[30,]=k10_sp_df_ea_s_w[,1]
Cconst[31,]=k10_sp_df_ea_s_w[,1]

Cconst[32,]=k11_sp_df_ea_s_w[,1]
Cconst[33,]=k11_sp_df_ea_s_w[,1]

Cconst[34,]=k12_sp_df_ea_s_w_egzwsf[,1]
Cconst[35,]=k12_sp_df_ea_s_w_egzwsf[,1]

Cconst[36,]=k13_sp_df_ea_s_w_egzwsf[,1]
Cconst[37,]=k13_sp_df_ea_s_w_egzwsf[,1]

Cconst[38,]=k14_sp_df_ea_s_w_egzwsf[,1]
Cconst[39,]=k14_sp_df_ea_s_w_egzwsf[,1]

Cconst[40,]=k15_sp_df_ea_s_w_egzwsf[,1]
Cconst[41,]=k15_sp_df_ea_s_w_egzwsf[,1]

Cconst[42,]=k16_sp_df_ea_s_w_egzwsf[,1]
Cconst[43,]=k16_sp_df_ea_s_w_egzwsf[,1]

Cconst[44,]=k17_sp_df_ea_s_w_egzwsf[,1]
Cconst[45,]=k17_sp_df_ea_s_w_egzwsf[,1]


lp_results=lp(direction="max", objective.in=COb, const.mat=Cconst, const.dir=Cdir, const.rhs=C_right_hand,all.bin=T)

lp_results$solution
ark1=ark1+1
#}


summary(lp_results$solution)
length(lp_results$solution)
image(lp_results$solution)
class(lp_results$solution)

#################################################################
# Clusterung
#################################################################

sg_sp=read.asciigrid("sgas100_clip_ascii.asc")# 6754 Siedlungsgebiet
sg_sp_df=as.data.frame(sg_sp)

#lp_output_nc_df_stand=data.frame(result=lp_results$solution,X=k1_sp_df_ea_s_w$X,Y=k1_sp_df_ea_s_w$Y)
#coordinates(lp_output_nc_df_stand)=~X+Y
#gridded(lp_output_nc_df_stand) = TRUE

#lp_output_nc_grid_stand = as(lp_output_nc_df_stand, "SpatialGridDataFrame") # to full grid

#write.asciigrid(lp_output_nc_grid_stand,"lp_results$solution_ascii.asc") # L�sung 

#lp_results$solution_sp=read.asciigrid("lp_results$solution_ascii.asc")

lp_results_solution_sp_df=data.frame(cell=lp_results$solution,X=k1_sp_df_ea_s_w$X,Y=k1_sp_df_ea_s_w$Y)      #4405 L�sung aus Modell

#--------------------------------------------------------- Analyse muss auf Fl�che mit Siedlungsgebiet erfolgen
#sg_sp_df # built area 6754
#lp_results_solution_sp_df #solution modell 4405

ONES_lp_results_solution_sp_df=which(lp_results_solution_sp_df$cell==1)

ZEROS_lp_results_solution_sp_df=which(lp_results_solution_sp_df$cell==0)



ONES_lp_results_solution_sp_SP=SpatialPoints(lp_results_solution_sp_df[ONES_lp_results_solution_sp_df,2:3])
sg_sp_SP=SpatialPoints(sg_sp_df[,2:3])

ZEROS_lp_results_solution_sp_SP=SpatialPoints(lp_results_solution_sp_df[ZEROS_lp_results_solution_sp_df,2:3])

overlap_layers_buildings_ones=zerodist2(sg_sp_SP,ONES_lp_results_solution_sp_SP)

overlap_layers_buildings_zeros=zerodist2(sg_sp_SP,ZEROS_lp_results_solution_sp_SP)


Modellloesung_mit_Siedlungsflaeche_df=sg_sp_df


#Modellloesung_mit_Siedlungsflaeche_df[overlap_layers_buildings_ones[,1],1]=1

Modellloesung_mit_Siedlungsflaeche_df_2=data.frame(index=rep(1:nrow(sg_sp_df)),type=rep(2,nrow(sg_sp_df)),Modellloesung_mit_Siedlungsflaeche_df)#f�r Geb�ude 2

Modellloesung_mit_Siedlungsflaeche_df_2$type[overlap_layers_buildings_ones[,1]]=3 #f�r Modelll�sungen3
Modellloesung_mit_Siedlungsflaeche_df_2$type[overlap_layers_buildings_zeros[,1]]=4 #f�r Modelll�sungen3


index_buildings=which(Modellloesung_mit_Siedlungsflaeche_df_2$type==2)
index_solution_ones=which(Modellloesung_mit_Siedlungsflaeche_df_2$type==3)
index_no_solution_zeros=which(Modellloesung_mit_Siedlungsflaeche_df_2$type==4)



#--------------------------------------------------------- Clusteranalyse
index_zeros_Modellloesung_mit_Siedlungsflaeche=which(Modellloesung_mit_Siedlungsflaeche_df$sgas100_clip_ascii.asc==0)
index_ones_Modellloesung_mit_Siedlungsflaeche=which(Modellloesung_mit_Siedlungsflaeche_df$sgas100_clip_ascii.asc==1)


ones_Modellloesung_mit_Siedlungsflaeche=Modellloesung_mit_Siedlungsflaeche_df[-index_zeros_Modellloesung_mit_Siedlungsflaeche,]

Modellloesung_mit_Siedlungsflaeche_SP=SpatialPoints(cbind(ones_Modellloesung_mit_Siedlungsflaeche$s1,ones_Modellloesung_mit_Siedlungsflaeche$s2))
Modellloesung_mit_Siedlungsflaeche_dnear=card(dnearneigh(Modellloesung_mit_Siedlungsflaeche_SP,0,201))  #Diestanz zu next Nachbar


table_with_coeff=data.frame(cluster_coeff=rep(999,nrow(sg_sp_df)),solution=rep(0,nrow(sg_sp_df)),X=sg_sp_df$s1,sg_sp_df$s2)

table_with_coeff$cluster_coeff[index_ones_Modellloesung_mit_Siedlungsflaeche]=Modellloesung_mit_Siedlungsflaeche_dnear



table_with_coeff$cluster_coeff[c(index_no_solution_zeros,index_buildings)]=999

index_unclustered=which(table_with_coeff$cluster_coeff==0)


unclustered_solution_DF=data.frame(cell=rep(0,nrow(sg_sp_df)),sg_sp_df[,2:3])

unclustered_solution_DF$cell[index_unclustered]=1

summary(as.factor(unclustered_solution_DF$cell))







#--------------------------------------------------------- Transformation zu Kriterium f�r Modell
kca_sp_df_ea=index_unclustered[ea1_overlap[,1],]
kca_sp_df_ea=data.frame(kca_sp_df_ea,X=k1_sp_df_ea_s$X,Y=k1_sp_df_ea_s$Y)



#################################################################
# Transformation f�r Export
#################################################################

#------------------------------------- Um das Resultat darzustellen
lp_output_nc_df_stand=data.frame(result=lp_results$solution,X=k1_sp_df_ea_s_w$X,Y=k1_sp_df_ea_s_w$Y)

coordinates(lp_output_nc_df_stand)=~X+Y
gridded(lp_output_nc_df_stand) = TRUE

lp_output_nc_grid_stand = as(lp_output_nc_df_stand, "SpatialGridDataFrame") # to full grid
image(lp_output_nc_grid_stand)

#------------------------------------- Export ascii File
write.asciigrid(lp_output_nc_grid_stand,"lp_results$solution_ascii.asc") # L�sung 
write.dbf(lp_output_nc_grid_stand,file="s111204Name.dbf")

#################################################################
# 6.2 objectiv function mit loops
#################################################################

#------------------------------------- (5.2)to set goal
C_right_hand=rep(0,43)

p=0
a=0
b=0
c=0
d=0
e=0

while(sum(lp_results$solution)==0){

#repeat until(sum(lp_results$solution)==1){

C_right_hand[11]=455-p

C_right_hand[2]=25-a #Ud 25
C_right_hand[3]=296-b #Th 296
C_right_hand[4] =59-c #SP 59
C_right_hand[5]=48-d #SB 48
C_right_hand[6]=27-e #HB 27

C_right_hand[7]=0
C_right_hand[8]=0
C_right_hand[9]=0
C_right_hand[10]=0
C_right_hand[11]=0


#################################################################
# 9 Aufaddierung f�r kontinuierliches Resultat
#################################################################

#------------------------------------- 11 Kriterien

added11k_sp_df_ea_s_w_a=data.frame((k_aufsummiert=((k1_sp_df_ea_s_w[,1])+(k2_sp_df_ea_s_w[,1])+(k3_sp_df_ea_s_w[,1])+(k4_sp_df_ea_s_w[,1])+(k5_sp_df_ea_s_w[,1])+(k6_sp_df_ea_s_w[,1])+(k7_sp_df_ea_s_w[,1])+(k8_sp_df_ea_s_w[,1])+(k9_sp_df_ea_s_w[,1])+(k10_sp_df_ea_s_w[,1])+(k11_sp_df_ea_s_w[,1]))),X=k1_sp_df_ea_s_w$X,Y=k1_sp_df_ea_s_w$Y)

added11k_sp_df_ea_s_w_a_m=data.frame(value=added11k_sp_df_ea_s_w_a,X=k1_sp_df_ea_s_w$X,Y=k1_sp_df_ea_s_w$Y) #braucht es um ein SpatialPixelsDataFrame zu bilden, welches mit image gemapped werden kann

coordinates(added11k_sp_df_ea_s_w_a_m)=~X+Y
gridded(added11k_sp_df_ea_s_w_a_m)= TRUE
added11k_sp_df_ea_s_w_a_m_grid = as(added11k_sp_df_ea_s_w_a_m, "SpatialGridDataFrame") # to full grid
image(added11k_sp_df_s_w_ea_a_m_grid)

write.asciigrid(added11k_sp_df_ea_s_w_a_m_grid,"added11k_sp_df_ea_s_w_a_m_grid.asc")

#nur Werte wo �berbaut werden soll
resultueberbau_sp_df_ea_s_w_a_m=data.frame(value=(added10k_sp_df_ea_s_w_a[,1]*lp_results$solution),X=k1_sp_df_ea_s_w$X,Y=k1_sp_df_ea_s_w$Y)

coordinates(resultueberbau_sp_df_ea_s_w_a_m)=~X+Y
gridded(resultueberbau_sp_df_ea_s_w_a_m)= TRUE
resultueberbau_sp_df_ea_s_w_a_m_grid = as(resultueberbau_sp_df_ea_s_w_a_m, "SpatialGridDataFrame") # to full grid
image(resultueberbau_sp_df_ea_s_w_a_m_grid)

#write.asciigrid(resultueberbau_sp_df_ea_s_w_a_m_grid,"resultueberbau_sp_df_ea_s_w_a_m_grid.asc")

#################################################################
# 10 tests
#################################################################

#------------------------------------- correlation
all_variables=data.frame(k1=k1_sp_df$cell,k2=k2_sp_df$cell,k3=k3_sp_df$cell,k4=k4_sp_df$cell,k5=k5_sp_df$cell,k6=k6_sp_df$cell,k7=k7_sp_df$cell,k8=k8_sp_df$cell,k9=k9_sp_df$cell,k10=k10_sp_df$cell,k11=k11_sp_df$cell,k12=k12_sp_df$cell,k13=k13_sp_df$cell)

correlation=cor(all_variables)

#------------------------------------- write table

write.csv(correlation,"correlation.csv",row.names = T)

write.dbf(correlation,"correlation.dbf")

#################################################################
# 11 Automation
#################################################################

# for (i 1 in 20) {

names(k_sp_df[i])=c("cell","X","Y")
}

#################################################################
# 12 Kriterientabelle erstellen
#################################################################
#------------------------------------- absolut
kriterientabelle=data.frame(k1=(k1_sp_df[,1]),k2=(k2_sp_df[,1]),k3=(k3_sp_df[,1]),k4=(k4_sp_df[,1]),k5=(k5_sp_df[,1]),k6=(k6_sp_df[,1]),k7=(k7_sp_df[,1]),k8=(k8_sp_df[,1]),k9=(k9_sp_df[,1]),k10=(k10_sp_df[,1]),k11=(k11_sp_df[,1]),k12=(k12_sp_df[,1]),k13=(k13_sp_df[,1]),k14=(k14_sp_df[,1]),X=k1_sp_df$X,Y=k1_sp_df$Y)

#------------------------------------- standardisiert
kriterientabelle=data.frame(k1=(k1_sp_df_s[,1]),k2=(k2_sp_df_s[,1]),k3=(k3_sp_df_s[,1]),k4=(k4_sp_df_s[,1]),k5=(k5_sp_df_s[,1]),k6=(k6_sp_df_s[,1]),k7=(k7_sp_df_s[,1]),k8=(k8_sp_df_s[,1]),k9=(k9_sp_df_s[,1]),k10=(k10_sp_df_s[,1]),k11=(k11_sp_df_s[,1]),k12=(k12_sp_df_s[,1]),k13=(k13_sp_df_s[,1]),k14=(k14_sp_df_s[,1]),X=k1_sp_df_s$X,Y=k1_sp_df_s$Y)

coordinates(kriterientabelle)=~X+Y
gridded(kriterientabelle) = TRUE
kriterientabelle = as(kriterientabelle, "SpatialGridDataFrame")
image(kriterientabelle)

library(foreign)
write.dbf(kriterientabelle,file="kriterientabellek14.dbf")

#write.csv(kriterientabelle,"kriterientabellek14.csv",row.names = T)

#################################################################
# Exportieren von L�sungen f�r Kriterientabelle
#################################################################

#------------------------------------- einlesen
m1re1c1_sp=read.asciigrid("m1re1c1_ascii.asc")
m1re1c1_sp_df=as.data.frame(m1re1c1_sp)
names(m1re1c1_sp_df)=c("cell","X","Y")

#------------------------------------- export
losungen=data.frame(m1re1c1=(m1re1c1_sp_df[,1]),m1mbrec1=(m1mbrec1_sp_df[,1]),m2rec=(m2rec_sp_df[,1]),m2mbrec=(m2mbrec_sp_df[,1]),m2mbocrec=(m2mbocrec_sp_df[,1]),m3rec=(m3rec_sp_df[,1]),m3mbrec=(m3mbrec_sp_df[,1]),m4rec=(m4rec_sp_df[,1]),m4mbrec=(m4mbrec_sp_df[,1]),m5rec=(m5rec_sp_df[,1]),m6rec=(m6rec_sp_df[,1]),m6mbrec=(m6mbrec_sp_df[,1]),m8rec=(m8rec_sp_df[,1]),X=k1_sp_df_s$X,Y=k1_sp_df_s$Y)

coordinates(losungen)=~X+Y
gridded(losungen) = TRUE
losung = as(losungen, "SpatialGridDataFrame")
image(losungen)

library(foreign)
write.dbf(losungen,file="loesungen.dbf")

#################################################################
# Exportieren von Daten
#################################################################

library(foreign)

k1_sp_df_id=data.frame(id=c(1:nrow(k1_sp_df)),k1_sp_df)
write.table(k1_sp_df_id,"k1_sp_df_id.dat",sep=",",row.names=F)

write.dbf(k1_sp_df_id,"k1_sp_df_id.dbf")
write.csv(k1_sp_df_id,"k1_sp_df_id.csv")
write.table(k1_sp_df_id,"k1_sp_df_id.dat",sep=",")

#################################################################
# Erstellen von dbf
#################################################################

lp1_sp=read.asciigrid("wbeig_kcodec_ascii.asc") # Nahrungsmittelproduktion
k1_sp_df=as.data.frame(k1_sp)
names(k1_sp_df)=c("cell","X","Y")

lp_output_nc_df_stand=data.frame(result=lp_results$solution,X=k1_sp_df_s_w_ea$X,Y=k1_sp_df_s_w_ea$Y)

coordinates(lp_output_nc_df_stand)=~X+Y
gridded(lp_output_nc_df_stand) = TRUE

lp_output_nc_grid_stand = as(lp_output_nc_df_stand, "SpatialGridDataFrame") # to full grid
image(lp_output_nc_grid_stand)

